package com.example.pizzas.home.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.pizzas.home.fargment.LogoutFragment
import com.example.pizzas.home.fargment.OrderPizzaFragment
import com.example.pizzas.home.model.Pizza

class BottomNavBarAdapter(private val fragment: FragmentManager, private val fragmentList: List<Fragment>): FragmentStatePagerAdapter(fragment, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getCount(): Int {
        return fragmentList.size
    }

    override fun getItem(position: Int): Fragment {
        return fragmentList[position]
    }

}