package com.example.pizzas.signup.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import com.example.pizzas.databinding.ActivitySignupBinding
import com.example.pizzas.R
import com.example.pizzas.extension.configureBackButtonWith
import com.example.pizzas.signup.viewModel.SignupViewModel

class SignupActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySignupBinding
    private val viewModel: SignupViewModel = SignupViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignupBinding.inflate(layoutInflater)
        setContentView(binding.root)
        configureBackButtonWith(getString(R.string.signup_navigation_title))
        binding.signupRegisterButton.setOnClickListener {
            val isUserInformationValid: Boolean = viewModel.validateUserInformation(binding.signupNameEditText.text.toString(),
                binding.signupEmailEditText.text.toString(),
                binding.signupPasswordEditText.text.toString(),
                binding.signupConfirmPasswordEditText.text.toString())
            if (isUserInformationValid) {
                viewModel.saveUserInDB { result ->
                    showMessage(viewModel.title, viewModel.message, action = {
                        if (result) {
                            finish()
                        }
                    })
                }
            } else {
                showMessage(viewModel.title, viewModel.message, action = {})
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun showMessage(title: String, message: String, action: () -> Unit) {
        val dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        dialogBuilder.setTitle(title)
        dialogBuilder.setMessage(message)
        dialogBuilder.setNeutralButton(getString(R.string.signup_alertDialog_neutral_button)) { _, _ ->
            action()
        }
        val dialog: AlertDialog = dialogBuilder.create()
        dialog.show()
    }
}