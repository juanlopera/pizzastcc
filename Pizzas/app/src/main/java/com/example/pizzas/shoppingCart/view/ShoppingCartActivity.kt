package com.example.pizzas.shoppingCart.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pizzas.R
import com.example.pizzas.confirmOrder.view.ConfirmOrderActivity
import com.example.pizzas.databinding.ActivityShoppingCartBinding
import com.example.pizzas.extension.configureBackButtonWith
import com.example.pizzas.shoppingCart.adapter.ShoppingCardListAdapter
import com.example.pizzas.shoppingCart.viewModel.ShoppingCarViewModel

class ShoppingCartActivity : AppCompatActivity() {

    private lateinit var binding: ActivityShoppingCartBinding
    private val viewModel: ShoppingCarViewModel = ShoppingCarViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityShoppingCartBinding.inflate(layoutInflater)
        setContentView(binding.root)
        handleClickEvents()
        configureBackButtonWith(getString(R.string.shopping_cart_nav_title))
    }

    override fun onResume() {
        super.onResume()
        getPreselectionList()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.shopping_cart_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menuDeleteShoppingCart) {
            viewModel.deleteShoppingCart()
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getPreselectionList() {
        viewModel.getPreselectedItems { result ->
            binding.totalTextView.text = viewModel.getTotal()
            if (result) {
                val shoppingListAdapter = ShoppingCardListAdapter(viewModel.preselectionList)
                binding.itemsRecycleView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
                binding.itemsRecycleView.adapter = shoppingListAdapter
            } else {
                showMessage(viewModel.titleError, viewModel.messageError) {
                    finish()
                }
            }
        }
    }

    private fun handleClickEvents() {
        binding.continueButton.setOnClickListener {
            val intent: Intent = Intent(this, ConfirmOrderActivity::class.java)
            startActivity(intent)
        }
    }

    private fun showMessage(title: String, message: String, action: () -> Unit) {
        val dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        dialogBuilder.setTitle(title)
        dialogBuilder.setMessage(message)
        dialogBuilder.setNeutralButton(getString(R.string.signup_alertDialog_neutral_button)) { _, _ ->
            action()
        }
        val dialog: AlertDialog = dialogBuilder.create()
        dialog.show()
    }
}