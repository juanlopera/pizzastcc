package com.example.pizzas.signup.viewModel
import android.util.Log
import com.example.pizzas.signup.model.CreateUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

internal class SignupViewModel {

    // MARK: - Private Properties
    private val minCharactersInPassword: Int = 4
    private var user: CreateUser? = null

    // MARK: - Computed Properties
    var title: String = ""
        private set
    var message: String = ""
        private set

    // MARK: Internal Function
    fun validateUserInformation(name: String,
                                email: String,
                                password: String,
                                confirmPassword: String): Boolean {
        title = "Erro"
        if (name.isEmpty() || email.isEmpty() || password.isEmpty() || confirmPassword.isEmpty()) {
            message = "Por favor preencher todos os campos."
            return false
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            message = "O E-Mail não é válido, por favor coloque um E-Mail válido."
            return false
        } else if (password.length < minCharactersInPassword) {
            message = "A senha deve conter no minimo 4 caracteres."
            return false
        } else if (password != confirmPassword) {
            message = "As senhas não coincidem."
            return false
        }
        user = CreateUser(name, email, password)
        return true
    }

    fun saveUserInDB(callback: (Boolean) -> Unit) {
        user?.let { user ->
            val userHas: HashMap<String, String> = hashMapOf(
                "name" to user.name,
                "email" to user.email,
                "password" to user.password
            )
            val dataBase: FirebaseFirestore = Firebase.firestore
            dataBase.collection("User")
                .add(userHas)
                .addOnSuccessListener { documentReference ->
                    Log.d("FSuccess", "DocumentSnapshot added with ID: ${documentReference.id}")
                    title = "Sucesso!"
                    message = "Usuario ficou salvo no banco de dados."
                    callback(true)
                }
                .addOnFailureListener { error ->
                    Log.w("FError", "Error adding document")
                    title = "Erro!"
                    message = "Acontenceu um erro ao momento de salvar o usuario no banco de dados, tente novamente."
                    callback(false)
                }
        }
    }
}