package com.example.pizzas.login.model

data class User(
    val id: String,
    var name: String,
    var email: String,
    var password: String
)
