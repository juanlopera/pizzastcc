package com.example.pizzas.home.fargment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pizzas.R
import com.example.pizzas.databinding.FragmentOrdersBinding
import com.example.pizzas.home.adapter.OrdersAdapter
import com.example.pizzas.home.model.Order
import com.example.pizzas.home.model.Pizza

class OrdersFragment(private val orderList: MutableList<Order>) : Fragment() {

    private lateinit var binding: FragmentOrdersBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentOrdersBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val orderAdapter: OrdersAdapter = OrdersAdapter(orderList)
        binding.ordersList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.ordersList.adapter = orderAdapter
    }
}