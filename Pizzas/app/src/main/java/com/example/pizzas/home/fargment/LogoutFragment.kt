package com.example.pizzas.home.fargment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.pizzas.R
import com.example.pizzas.databinding.FragmentLogoutBinding
import com.example.pizzas.sessionManager.SessionManager

class LogoutFragment : Fragment() {


    private lateinit var binding: FragmentLogoutBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentLogoutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.customerName.text = SessionManager.username
        binding.logoutButton.setOnClickListener {
            requireActivity().finish()
        }
    }
}