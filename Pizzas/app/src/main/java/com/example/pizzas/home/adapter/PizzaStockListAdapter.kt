package com.example.pizzas.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pizzas.databinding.PizzaStockItemBinding
import com.example.pizzas.home.model.Pizza

class PizzaStockListAdapter(val pizzaList: List<Pizza>): RecyclerView.Adapter<PizzaStockListViewHolderBase>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PizzaStockListViewHolderBase {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)
        return PizzaStockListViewHolderBase.PizzaItem(PizzaStockItemBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: PizzaStockListViewHolderBase, position: Int) {
        val pizza: Pizza = pizzaList[position]
        if (holder is PizzaStockListViewHolderBase.PizzaItem) {
            holder.bind(pizza)
        }
    }

    override fun getItemCount(): Int {
        return pizzaList.size
    }
}