package com.example.pizzas.home.model

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class Pizza(var id: String,
                 var name: String,
                 var description: String,
                 var price: Double,
                 var stock: Int): Parcelable
