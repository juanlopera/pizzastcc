package com.example.pizzas.orderPizza.viewModel

import com.example.pizzas.home.model.Pizza
import com.example.pizzas.sessionManager.SessionManager
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class OrderPizzaViewModel {

    lateinit var pizza: Pizza

    // MARK: - Computed Properties
    var titleError: String = "Erro"
        private set

    var messageError: String = "Ops, aconteceu um erro ao momento de obter a informação do nosso servidor."
        private set

    fun preselectPizza(preselectAmount: String, callback: (Boolean) -> Unit) {
        var preselection: String = preselectAmount
        if (preselectAmount.isNullOrEmpty()) {
            preselection = "0"
        }
        if (preselection.matches(Regex("[0-9]+"))) {
            val amount: Int = preselection.toInt()
            savePreselection(amount, callback)
        } else {
            titleError = "Erro"
            messageError = "Ingresa solo numeros porfavor"
            callback(false)
        }
    }

    private fun savePreselection(amount: Int, callback: (Boolean) -> Unit) {
        if (amount <= 0) {
            titleError = "Erro"
            messageError = "A quantidade deve ser maior de 0"
            callback(false)
        } else if (pizza == null) {
            titleError = "Erro"
            messageError = "Ops, aconteceu um erro ao momento de obter a informação do nosso servidor."
            callback(false)
        } else if (amount <= pizza.stock) {
            val sdf = SimpleDateFormat("dd/M/yyyy HH:mm:ss")
            val currentDate = sdf.format(Date())
            val userHas: HashMap<String, Any> = hashMapOf(
                "id" to currentDate,
                "idPizza" to pizza.id,
                "name" to pizza.name,
                "stock" to pizza.stock,
                "preselectionAmount" to amount,
                "price" to pizza.price
            )
            val dataBase: FirebaseFirestore = Firebase.firestore
            dataBase.collection("Preselection")
                .add(userHas)
                .addOnSuccessListener { document ->
                    SessionManager.preselectionIds.add(document.id)
                    titleError = "Sucesso!"
                    messageError = "Sucesso no momento de separar sua seleção de pizza."
                    callback(true)
                }
                .addOnFailureListener {
                    titleError = "Erro"
                    messageError = "Ops, aconteceu um erro ao momento de obter a informação do nosso servidor."
                    callback(false)
                }
        } else {
            titleError = "Erro"
            messageError = "Atualmente temos ${pizza.stock} pizzas de ${pizza.name} disponiveis."
            callback(false)
        }
    }
}