package com.example.pizzas.extension

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

fun Date.dateToString(format: String): String {
    val dateFormatter = SimpleDateFormat(format, Locale.getDefault())
    return dateFormatter.format(this)
}