package com.example.pizzas.shoppingCart.adapter

import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.example.pizzas.databinding.ShoppingCardItemBinding
import com.example.pizzas.home.model.Preselection

sealed class ShoppingCardListViewHolderBase(binding: ViewBinding): RecyclerView.ViewHolder(binding.root) {
    class ShoppingCardItem(private val binding: ShoppingCardItemBinding): ShoppingCardListViewHolderBase(binding) {

        fun bind(preselection: Preselection) {
            binding.apply {
                binding.titleTextView.text = "X${preselection.preselectionAmount} ${preselection.name}"
                binding.descriptionItemTextView4.text = "R$ ${preselection.preselectionAmount.toDouble() * preselection.price}"
            }
        }
    }
}