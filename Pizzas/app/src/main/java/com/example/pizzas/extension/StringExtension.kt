package com.example.pizzas.extension

import android.os.Build
import androidx.annotation.RequiresApi
import java.text.SimpleDateFormat
import java.util.*

@RequiresApi(Build.VERSION_CODES.O)
fun String.convertToData(format: String): Date {
    val formatter = SimpleDateFormat(format, Locale.getDefault())
    return formatter.parse(this)
}