package com.example.pizzas.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pizzas.databinding.OrderItemBinding
import com.example.pizzas.home.model.Order

class OrdersAdapter(val orderList: List<Order>): RecyclerView.Adapter<OrdersAdaptarViewHolderBase>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrdersAdaptarViewHolderBase {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)
        return OrdersAdaptarViewHolderBase.OrderItem(OrderItemBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: OrdersAdaptarViewHolderBase, position: Int) {
        val order: Order = orderList[position]
        if (holder is OrdersAdaptarViewHolderBase.OrderItem) {
            holder.bind(order, position)
        }
    }

    override fun getItemCount(): Int {
        return orderList.size
    }
}