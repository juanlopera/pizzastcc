package com.example.pizzas.home.fargment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pizzas.R
import com.example.pizzas.databinding.FragmentOrderPizzaBinding
import com.example.pizzas.home.adapter.PizzaListAdapter
import com.example.pizzas.home.adapter.PizzaSelected
import com.example.pizzas.home.model.Pizza
import com.example.pizzas.orderPizza.view.OrderPizzaActivity
import com.example.pizzas.sessionManager.SessionManager
import com.example.pizzas.shoppingCart.view.ShoppingCartActivity

class OrderPizzaFragment(private val pizzaList: MutableList<Pizza>) : Fragment() {

    private lateinit var binding: FragmentOrderPizzaBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentOrderPizzaBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleClickEvent()
        val pizzaAdapter: PizzaListAdapter = PizzaListAdapter(pizzaList)
        binding.pizzaList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.pizzaList.adapter = pizzaAdapter
        pizzaAdapter.setListener(object : PizzaSelected {
            override fun onClickInPizza(position: Int) {
                val intent: Intent = Intent(requireContext(), OrderPizzaActivity::class.java)
                intent.putExtra("PizzaSelected", pizzaList[position])
                startActivity(intent)
            }
        })
    }

    private fun handleClickEvent() {
        binding.buyCar.setOnClickListener {
            if (SessionManager.preselectionIds.isEmpty()) {
                showMessage("Ops!", "Não tem nada no carrinho de compras") {}
            } else {
                val intent: Intent = Intent(requireContext(), ShoppingCartActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun showMessage(title: String, message: String, action: () -> Unit) {
        val dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(requireContext())
        dialogBuilder.setTitle(title)
        dialogBuilder.setMessage(message)
        dialogBuilder.setNeutralButton(getString(R.string.signup_alertDialog_neutral_button)) { _, _ ->
            action()
        }
        val dialog: AlertDialog = dialogBuilder.create()
        dialog.show()
    }
}