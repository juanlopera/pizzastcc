package com.example.pizzas.sessionManager

object SessionManager {
    var username: String = ""
    var userEmail: String = ""
    var preselectionIds: ArrayList<String> = ArrayList()
}