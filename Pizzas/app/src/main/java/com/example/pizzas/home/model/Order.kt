package com.example.pizzas.home.model

data class Order(
    val date: String,
    val cep: String,
    val customerName: String,
    val status: String,
    val items: ArrayList<String>,
    val paymentMethod: String,
    val total: String
)
