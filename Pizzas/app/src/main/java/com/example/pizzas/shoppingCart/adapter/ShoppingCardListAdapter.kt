package com.example.pizzas.shoppingCart.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pizzas.databinding.ShoppingCardItemBinding
import com.example.pizzas.home.model.Preselection

class ShoppingCardListAdapter(val preselectionList: List<Preselection>): RecyclerView.Adapter<ShoppingCardListViewHolderBase>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShoppingCardListViewHolderBase {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)
        return ShoppingCardListViewHolderBase.ShoppingCardItem(ShoppingCardItemBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: ShoppingCardListViewHolderBase, position: Int) {
        val preselection: Preselection = preselectionList[position]
        if (holder is ShoppingCardListViewHolderBase.ShoppingCardItem) {
            holder.bind(preselection)
        }
    }

    override fun getItemCount(): Int {
        return preselectionList.size
    }
}