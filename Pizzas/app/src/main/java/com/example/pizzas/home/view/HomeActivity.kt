package com.example.pizzas.home.view

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.example.pizzas.R
import com.example.pizzas.databinding.ActivityHomeBinding
import com.example.pizzas.home.adapter.BottomNavBarAdapter
import com.example.pizzas.home.fargment.LogoutFragment
import com.example.pizzas.home.fargment.OrderPizzaFragment
import com.example.pizzas.home.fargment.OrdersFragment
import com.example.pizzas.home.fargment.StockPizzasFragment
import com.example.pizzas.home.viewModel.HomeViewModel

class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding
    private var viewModel: HomeViewModel = HomeViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        bottomNavigationItemSelected()
        setViewPagerSwipeListerner()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onResume() {
        super.onResume()
        getPizzas()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getPizzas() {
        viewModel.getOrdersPizza()
        viewModel.getPizzaList({ result ->
            if (result) {
                val bottomNavBarAdapter: BottomNavBarAdapter = BottomNavBarAdapter(supportFragmentManager, getFragmentList())
                binding.viewPager.adapter = bottomNavBarAdapter
            } else {
                showMessage(viewModel.titleError, viewModel.messageError)
            }
        }, {
            showMessage("Tempo excedido",
                "A lista de preseleção vai ser deletada porque excedeu o tempo maximo de 5 min.",
                {})
        })
    }

    private fun bottomNavigationItemSelected() {
        binding.menu.setOnItemSelectedListener { item ->
            binding.viewPager.currentItem = when(item.itemId) {
                R.id.orderPizza ->  0
                R.id.stock -> 1
                R.id.orders -> 2
                else -> 3
            }
            true
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun showMessage(title: String, message: String) {
        val dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        dialogBuilder.setTitle(title)
        dialogBuilder.setMessage(message)
        dialogBuilder.setPositiveButton("Tentar novamente") { _, _ ->
            getPizzas()
        }
        dialogBuilder.setNegativeButton("Sair") { _, _ ->
            finish()
        }

        val dialog: AlertDialog = dialogBuilder.create()
        dialog.show()
    }

    private fun showMessage(title: String, message: String, action: () -> Unit) {
        val dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        dialogBuilder.setTitle(title)
        dialogBuilder.setMessage(message)
        dialogBuilder.setNeutralButton(getString(R.string.signup_alertDialog_neutral_button)) { _, _ ->
            action()
        }
        val dialog: AlertDialog = dialogBuilder.create()
        dialog.show()
    }

    private fun setViewPagerSwipeListerner() {
        binding.viewPager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {}

            override fun onPageSelected(position: Int) {
                binding.menu.selectedItemId = when(position) {
                    0 -> R.id.orderPizza
                    1 -> R.id.stock
                    2 -> R.id.orders
                    else -> R.id.logout
                }
            }
        })
    }

    private fun getFragmentList(): List<Fragment> {
        return listOf(
            OrderPizzaFragment(viewModel.pizzaList),
            StockPizzasFragment(viewModel.pizzaList),
            OrdersFragment(viewModel.orderPizzaList),
            LogoutFragment()
        )
    }
}