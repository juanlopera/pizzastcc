package com.example.pizzas.home.model

data class Preselection(
    val name: String,
    val preselectionAmount: Int,
    val idPizza: String,
    val id: String,
    val stock: Int,
    val price: Double
)