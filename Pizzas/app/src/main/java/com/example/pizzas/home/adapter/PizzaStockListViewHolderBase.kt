package com.example.pizzas.home.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.example.pizzas.databinding.PizzaStockItemBinding
import com.example.pizzas.home.model.Pizza

sealed class PizzaStockListViewHolderBase(binding: ViewBinding): RecyclerView.ViewHolder(binding.root) {
    class PizzaItem(private val binding: PizzaStockItemBinding): PizzaStockListViewHolderBase(binding) {

        fun bind(pizza: Pizza) {
            binding.apply {
                pizzaTitleStock.text = pizza.name
                pizzaCounterStock.text = "Quantidade disponivel ${pizza.stock}"
                if (pizza.stock > 0) {
                    circleGreenImageView.visibility = View.VISIBLE
                    circleRedImageView.visibility = View.INVISIBLE
                } else {
                    circleGreenImageView.visibility = View.INVISIBLE
                    circleRedImageView.visibility = View.VISIBLE
                }
            }
        }
    }
}