package com.example.pizzas.login.viewModel

import android.util.Log
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.example.pizzas.login.model.User
import com.example.pizzas.sessionManager.SessionManager
import com.google.firebase.firestore.QuerySnapshot

internal class MainViewModel {

    // MARK: - Private Properties
    var userList: MutableList<User> = mutableListOf()

    // MARK: - Computed Properties
    var title: String = ""
        private set
    var message: String = ""
        private set

    // MARK: - Internal Function
    fun validateUserCredentials(email: String, password: String, callback: (Boolean) -> Unit) {
        if (email.isEmpty() || password.isEmpty()) {
            title = "Erro"
            message = "Por favor, preencher todos os campos"
            callback(false)
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            title = "Erro"
            message = "E-Mail no formato inválido, por favor coloque um E-Mail com formato certo."
            callback(false)
        } else if (password.length < 4) {
            title = "Erro"
            message = "A senha deve conter no minimo 4 caracters."
            callback(false)
        } else {
            val dataBase: FirebaseFirestore = Firebase.firestore
            dataBase.collection("User")
                .get()
                .addOnSuccessListener { result ->
                    fillUserList(result)
                    if (areUserCredentialsCorrect(email, password)) {
                        callback(true)
                    } else {
                        title = "Erro nas credenciais"
                        message = "Erro nas credenciais"
                        callback(false)
                    }
                }
                .addOnFailureListener { _ ->
                    title = "Erro nas credenciais"
                    message = "Erro nas credenciais"
                    callback(false)
                }
        }
    }

    private fun fillUserList(result: QuerySnapshot) {
        for (document in result) {
            val id: String = document.id
            val email: String? = document.data.get("email") as? String
            val password: String? = document.data.get("password") as? String
            val name: String? = document.data.get("name") as? String
            var newUser: User = User(id, "", "", "")
            email?.let { emailSafe ->
                newUser.email = emailSafe
            }
            password?.let { passwordSafe ->
                newUser.password = passwordSafe
            }
            name?.let { nameSafe ->
                newUser.name = nameSafe
            }
            userList.add(newUser)
        }
    }

    private fun areUserCredentialsCorrect(email: String, password: String): Boolean {
        val listFilter: List<User> = userList.filter { user ->
            user.email == email && user.password == password
        }
        if (!listFilter.isEmpty()) {
            SessionManager.userEmail = listFilter.last().email
            SessionManager.username  = listFilter.last().name
        }
        return !listFilter.isEmpty()
    }
}