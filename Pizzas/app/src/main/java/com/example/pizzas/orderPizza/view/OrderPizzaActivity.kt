package com.example.pizzas.orderPizza.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import com.example.pizzas.R
import com.example.pizzas.databinding.ActivityOrderPizzaBinding
import com.example.pizzas.extension.configureBackButtonWith
import com.example.pizzas.home.model.Pizza
import com.example.pizzas.orderPizza.viewModel.OrderPizzaViewModel

class OrderPizzaActivity : AppCompatActivity() {

    private lateinit var binding: ActivityOrderPizzaBinding
    private val viewModel: OrderPizzaViewModel = OrderPizzaViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOrderPizzaBinding.inflate(layoutInflater)
        setContentView(binding.root)
        configureBackButtonWith(getString(R.string.order_pizza_nav_title))
        recoverInformation()
        handleClickEvents()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun recoverInformation() {
        val pizzaSelected: Pizza? = intent.getParcelableExtra("PizzaSelected") as? Pizza
        pizzaSelected?.let { pizza ->
            viewModel.pizza = pizza
            binding.pizzaNameTextView.text = pizza.name
            binding.descriptionTextView.text = pizza.description
            binding.imageView.setImageDrawable(resources.getDrawable(R.drawable.frango_catupiry))
        }
    }

    private fun handleClickEvents() {
        binding.addButton.setOnClickListener {
            viewModel.preselectPizza(binding.editTextNumber.text.toString()) { result ->
                showMessage(viewModel.titleError, viewModel.messageError) {
                    if (result) {
                        finish()
                    }
                }
            }
        }
    }

    private fun showMessage(title: String, message: String, action: () -> Unit) {
        val dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        dialogBuilder.setTitle(title)
        dialogBuilder.setMessage(message)
        dialogBuilder.setNeutralButton(getString(R.string.signup_alertDialog_neutral_button)) { _, _ ->
            action()
        }
        val dialog: AlertDialog = dialogBuilder.create()
        dialog.show()
    }
}