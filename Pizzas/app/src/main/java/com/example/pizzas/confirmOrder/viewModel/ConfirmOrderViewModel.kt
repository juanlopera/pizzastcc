package com.example.pizzas.confirmOrder.viewModel

import android.util.Log
import com.example.pizzas.home.model.Preselection
import com.example.pizzas.sessionManager.SessionManager
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ConfirmOrderViewModel {

    var preselectionList: ArrayList<Preselection> = ArrayList()

    var titleError: String = "Erro"
        private set

    var messageError: String = "Ops, aconteceu um erro ao momento de obter a informação do nosso servidor."
        private set

    fun getTotal(): String {
        var total: Double = 0.0
        preselectionList.forEach { preselection ->
            total += (preselection.price * preselection.preselectionAmount.toDouble())
        }
        return "Total: R$ ${total}"
    }

    fun getPreselectedItems(callback: (Boolean) -> Unit) {
        val dataBase: FirebaseFirestore = Firebase.firestore
        dataBase.collection("Preselection")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    if (SessionManager.preselectionIds.contains(document.id)) {
                        val preselection: Preselection = Preselection(
                            document.data["name"] as String,
                            (document.data["preselectionAmount"] as Long).toInt(),
                            document.data["idPizza"] as String,
                            document.data["id"] as String,
                            (document.data["stock"] as Long).toInt(),
                            (document.data["price"] as Double)
                        )
                        preselectionList.add(preselection)
                    }
                }
                callback(true)
            }
            .addOnFailureListener {
                titleError = "Erro"
                messageError = "Ops, aconteceu um erro ao momento de obter a informação do nosso servidor."
                callback(false)
            }
    }

    fun validateInputs(customerName: String, cep: String, paymentMethod: String): Boolean {
        if (customerName.isNullOrEmpty()) {
            titleError = "Erro"
            messageError = "O nome do cliente não pode ficar vazio."
            return false
        }
        if (cep.isNullOrEmpty()) {
            titleError = "Erro"
            messageError = "O CEP de entrega não pode ficar vazio."
            return false
        }
        if (paymentMethod.isNullOrEmpty()) {
            titleError = "Erro"
            messageError = "Informe a forma de pagamento."
            return false
        }
        return true
    }

    fun createOrder(customerName: String, cep: String, paymentMethod: String, callback: (Boolean) -> Unit) {
        val sdf = SimpleDateFormat("dd/M/yyyy HH:mm:ss")
        val currentDate = sdf.format(Date())
        val order: HashMap<String, Any> = hashMapOf(
            "date" to currentDate,
            "cep" to cep,
            "customerName" to customerName,
            "paymentMethod" to paymentMethod,
            "status" to "pedido em preparação",
            "total" to getTotal()
        )
        var items: ArrayList<String> = ArrayList()
        preselectionList.forEachIndexed { index, preselection ->
            items.add("X${preselection.preselectionAmount} ${preselection.name}")
        }
        order.put("items", items)
        val dataBase: FirebaseFirestore = Firebase.firestore
        dataBase.collection("PizzaOrders")
            .add(order)
            .addOnSuccessListener {
                titleError = "Sucesso!"
                messageError = "Pedido criado com sucesso!"
                updateStock(dataBase, callback)
            }
            .addOnFailureListener {
                titleError = "Erro"
                messageError = "Aconteceu um erro ao momento de criar o pedido."
                callback(false)
            }
    }

    private fun updateStock(dataBase: FirebaseFirestore, callback: (Boolean) -> Unit) {
        if (preselectionList.isNotEmpty()) {
            val lastItem = preselectionList.last()
            val productItem: HashMap<String, Any> = hashMapOf(
                "name" to lastItem.name,
                "id" to lastItem.idPizza,
                "price" to lastItem.price,
                "stock" to lastItem.stock - lastItem.preselectionAmount,
                "description" to "Massa tradicional super deliciosa ${lastItem.name}"
            )
            val product: HashMap<String, Any> = hashMapOf(
                lastItem.name to productItem
            )
            preselectionList.remove(lastItem)
            dataBase.collection("Product").document("TnO30fOwkCWYGGGySUQn")
                .update(product)
                .addOnSuccessListener {
                    updateStock(dataBase, callback)
                }
                .addOnFailureListener {
                    callback(false)
                }
        } else {
            callback(true)
        }
    }

    fun deleteShoppingCart() {
        SessionManager.preselectionIds.forEach { idPreselection ->
            val dataBase: FirebaseFirestore = Firebase.firestore
            dataBase.collection("Preselection").document(idPreselection)
                .delete()
                .addOnSuccessListener {
                    SessionManager.preselectionIds.remove(idPreselection)
                    Log.d("", "Preselection deleted")
                }
                .addOnFailureListener {
                    SessionManager.preselectionIds.remove(idPreselection)
                    Log.d("", "Preselection not deleted")
                }
        }
    }
}