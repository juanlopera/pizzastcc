package com.example.pizzas.signup.model

data class CreateUser(
    val name: String,
    val email: String,
    val password: String)
