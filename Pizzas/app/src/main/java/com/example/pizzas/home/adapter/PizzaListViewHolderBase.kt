package com.example.pizzas.home.adapter

import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.example.pizzas.databinding.PizzaItemBinding
import com.example.pizzas.home.model.Pizza
import com.example.pizzas.R

sealed class PizzaListViewHolderBase(binding: ViewBinding): RecyclerView.ViewHolder(binding.root) {
    class PizzaItem(private val binding: PizzaItemBinding, listener: PizzaSelected): PizzaListViewHolderBase(binding) {

        init {
            binding.root.setOnClickListener {
                listener.onClickInPizza(adapterPosition)
            }
        }

        fun bind(pizza: Pizza) {
            binding.apply {
                pizzaTitle.text = pizza.name
                pizzaDescription.text = pizza.description
                pizzaPrice.text = "R$ ${pizza.price}"
                val stock: Int = pizza.stock
                if (stock > 0) {
                    pizzaStatus.text = "Disponível"
                } else {
                    pizzaStatus.text = "Esgotado"
                }
            }
        }
    }
}