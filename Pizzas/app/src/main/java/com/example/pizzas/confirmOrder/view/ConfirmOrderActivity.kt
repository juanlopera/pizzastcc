package com.example.pizzas.confirmOrder.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.example.pizzas.R
import com.example.pizzas.confirmOrder.viewModel.ConfirmOrderViewModel
import com.example.pizzas.databinding.ActivityConfirmOrderBinding
import com.example.pizzas.extension.configureBackButtonWith
import com.example.pizzas.home.view.HomeActivity

class ConfirmOrderActivity : AppCompatActivity() {

    private lateinit var binding: ActivityConfirmOrderBinding
    private val viewModel: ConfirmOrderViewModel = ConfirmOrderViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityConfirmOrderBinding.inflate(layoutInflater)
        setContentView(binding.root)
        configureBackButtonWith(getString(R.string.confirm_order_nav_title))
        viewModel.getPreselectedItems { result ->
            if (!result) {
                showMessage(viewModel.titleError, viewModel.messageError) {
                    finish()
                }
            }
        }
        handleClickEvent()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun handleClickEvent() {
        binding.confirmOrderButton.setOnClickListener {
            val customerName: String = binding.customerNameTextView.text.toString()
            val cep: String = binding.cepTextView.text.toString()
            val paymentMethod: String = getPaymentMethod()
            if (viewModel.validateInputs(customerName, cep, paymentMethod)) {
                viewModel.createOrder(customerName, cep, paymentMethod) { result ->
                    if (result) {
                        viewModel.deleteShoppingCart()
                        val intent: Intent = Intent(this, HomeActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                    } else {
                        showMessage(viewModel.titleError, viewModel.messageError) {}
                    }
                }
            } else {
                showMessage(viewModel.titleError, viewModel.messageError) {}
            }
        }
    }

    private fun getPaymentMethod(): String {
        if (binding.paymentMethodRadioGroup.checkedRadioButtonId == binding.cardRadioButton.id) {
            return "Cartão"
        } else {
            return "A vista"
        }
    }

    private fun showMessage(title: String, message: String, action: () -> Unit) {
        val dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        dialogBuilder.setTitle(title)
        dialogBuilder.setMessage(message)
        dialogBuilder.setNeutralButton(getString(R.string.signup_alertDialog_neutral_button)) { _, _ ->
            action()
        }
        val dialog: AlertDialog = dialogBuilder.create()
        dialog.show()
    }
}