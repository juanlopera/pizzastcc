package com.example.pizzas.updateAddProduct.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import com.example.pizzas.R
import com.example.pizzas.databinding.ActivityUpdateAddProductBinding
import com.example.pizzas.extension.configureBackButtonWith
import com.example.pizzas.updateAddProduct.viewModel.UpdateAddProductViewModel
import kotlinx.android.synthetic.main.activity_update_add_product.*

class UpdateAddProductActivity : AppCompatActivity() {

    private lateinit var binding: ActivityUpdateAddProductBinding
    private val viewModel: UpdateAddProductViewModel = UpdateAddProductViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUpdateAddProductBinding.inflate(layoutInflater)
        setContentView(binding.root)
        configureBackButtonWith(String())
        recoveryInformation()
        setupUI()
        handleClickEvent()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun recoveryInformation() {
        viewModel.isUpdating = intent.getBooleanExtra("updating", false)
        viewModel.pizzaList = intent.getParcelableArrayListExtra("pizzaUpdating")
    }

    private fun setupUI() {
        binding.apply {
            nameProduct.isVisible = viewModel.isUpdating.not()
            descriptionProduct.isVisible = viewModel.isUpdating.not()
            priceProduct.isVisible = viewModel.isUpdating.not()
            stockProduct.isVisible = viewModel.isUpdating.not()

            nameProduct.isEnabled = viewModel.isUpdating.not()
            descriptionProduct.isEnabled = viewModel.isUpdating.not()
            priceProduct.isEnabled = viewModel.isUpdating.not()
            stockProduct.isEnabled = viewModel.isUpdating.not()
        }

        if (viewModel.isUpdating) {
            binding.idProduct.setOnEditorActionListener { _, id, keyEvent ->
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_ACTION_SEARCH || keyEvent == null || keyEvent.keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (viewModel.exist(binding.idProduct.text.toString()).not()) {
                        showMessage("Erro",
                            "O id não existe, por favor digite um id existente no banco de dados.",
                            {
                                showUpdatingInformation(false)
                            })
                    } else {
                        showUpdatingInformation(true)
                    }
                    true
                }
                false
            }
        }
    }

    private fun showUpdatingInformation(shouldShow: Boolean) {
        binding.apply {
            nameProduct.isVisible = shouldShow
            descriptionProduct.isVisible = shouldShow
            priceProduct.isVisible = shouldShow
            stockProduct.isVisible = shouldShow

            descriptionProduct.isEnabled = shouldShow
            priceProduct.isEnabled = shouldShow
            stockProduct.isEnabled = shouldShow

            nameProduct.setText(viewModel.pizza?.name ?: "")
            descriptionProduct.setText(viewModel.pizza?.description ?: "")
            priceProduct.setText(viewModel.pizza?.price.toString() ?: "")
            stockProduct.setText(viewModel.pizza?.stock.toString() ?: "")
        }
    }

    private fun showMessage(title: String, message: String, action: () -> Unit) {
        val dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        dialogBuilder.setTitle(title)
        dialogBuilder.setMessage(message)
        dialogBuilder.setNeutralButton(getString(R.string.signup_alertDialog_neutral_button)) { _, _ ->
            action()
        }
        val dialog: AlertDialog = dialogBuilder.create()
        dialog.show()
    }

    private fun handleClickEvent() {
        binding.finishBtn.setOnClickListener {
            if (viewModel.isUpdating) {
                finishWhenIsUpdating()
            } else {
                finishWhenIsAddingNewProduct()
            }
        }
    }

    private fun finishWhenIsUpdating() {
        val id: String = binding.idProduct.text.toString()
        val stock: String = binding.stockProduct.text.toString()
        val description: String = binding.descriptionProduct.text.toString()
        val price: String = binding.priceProduct.text.toString()
        if (viewModel.validateUpdatingInfo(id,
                stock, description, price)) {
            viewModel.updateInformation(stock, description, price) { result ->
                if (result) {
                    showMessage("Sucesso!",
                        "Produto atualizado com sucesso.",
                        {
                            finish()
                        })
                } else {
                    showMessage("Erro",
                        "Aconteceu um erro ao momento de atualizar, por favor tente novamente.",
                        {
                            finish()
                        })
                }
            }
        } else {
            showMessage("Erro",
                "Lembre de não deixar campos vazios e tambem de digitar so numeros no campo de estoque e id.",
                {})
        }
    }

    private fun finishWhenIsAddingNewProduct() {
        val id: String = binding.idProduct.text.toString()
        val name: String = binding.nameProduct.text.toString()
        val stock: String = binding.stockProduct.text.toString()
        val description: String = binding.descriptionProduct.text.toString()
        val price: String = binding.priceProduct.text.toString()
        if (viewModel.validateAddNewProductInfoNotEmpty(id, name, stock, description, price).not()) {
            showMessage("Erro",
                "Não podem haver campos vazios.",
                {})
            return
        }
        if (id.matches(Regex("[0-9]+")).not() && stock.matches(Regex("[0-9]+")).not() && price.matches(Regex("[0-9]+(\\.\\d+)")).not()) {
            showMessage("Erro",
                "Lembra que o campo id, preço e estoque só devem conter números.",
                {})
            return
        }
        if (viewModel.exist(id)) {
            showMessage("Erro",
                "O id já esta em uso, tenta com outro id, lembre-se que o id deve ser só números.",
                {})
            return
        } else {
            viewModel.createNewProduct(id, name, stock, description, price, { result ->
                if (result) {
                    showMessage("Sucesso",
                        "O novo produto foi criado com sucesso!",
                        {
                            finish()
                        })
                } else {
                    showMessage("Erro",
                        "Aconteceu um erro ao momento de criar o novo produto, por favor tenta novamente.",
                        {
                            finish()
                        })
                }
            })
        }
    }
}