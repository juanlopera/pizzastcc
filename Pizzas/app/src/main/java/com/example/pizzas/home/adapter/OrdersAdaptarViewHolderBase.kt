package com.example.pizzas.home.adapter

import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.example.pizzas.databinding.OrderItemBinding
import com.example.pizzas.home.model.Order

sealed class OrdersAdaptarViewHolderBase(binding: ViewBinding): RecyclerView.ViewHolder(binding.root) {
    class OrderItem(private val binding: OrderItemBinding): OrdersAdaptarViewHolderBase(binding) {

        fun bind(order: Order, position: Int) {
            binding.apply {
                cepOrderTextView.text = "Estado: ${order.status}"
                dataTextView.text = "Criado em: ${order.date}"
                customerOrderTextView.text = "Cliente: ${order.customerName}"
                totalOrderTextView.text = order.total
                var items: String = ""
                order.items.forEach { item ->
                    items = "${items} ${item}\n"
                }
                itemsOrderTextView.text = items
                numberOrderTextView.text = "${position + 1}"
            }
        }
    }
}