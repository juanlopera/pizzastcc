package com.example.pizzas.updateAddProduct.viewModel

import androidx.core.text.isDigitsOnly
import com.example.pizzas.home.model.Pizza
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class UpdateAddProductViewModel {

    var isUpdating: Boolean = false
    var pizzaList: ArrayList<Pizza>? = null
    var pizza: Pizza? = null

    fun exist(id: String): Boolean {
        if (pizzaList?.isEmpty() ?: true) {
            return false
        } else {
            pizzaList?.forEach { pizza ->
                if (pizza.id == id) {
                    this.pizza = pizza
                    return  true
                }
            }
        }
        return false
    }

    fun validateUpdatingInfo(id: String, stock: String, description: String, price: String): Boolean {
        return id.isNotEmpty() && stock.isNotEmpty() && stock.matches(Regex("[0-9]+")) && description.isNotEmpty() && price.isNotEmpty() && price.matches(Regex("[0-9]+(\\.\\d+)"))
    }

    fun validateAddNewProductInfoNotEmpty(id: String, name: String, stock: String, description: String, price: String): Boolean {
        return id.isNotEmpty() && name.isNotEmpty() && stock.isNotEmpty() && stock.matches(Regex("[0-9]+")) && description.isNotEmpty() && price.isNotEmpty() && price.matches(Regex("[0-9]+(\\.\\d+)"))
    }

    fun updateInformation(stock: String, description: String, price: String, callback: (Boolean) -> Unit) {
        val stockNumber: Int = stock.toInt()
        val productItem: HashMap<String, Any> = hashMapOf(
            "name" to pizza!!.name,
            "id" to pizza!!.id,
            "price" to price.toDouble(),
            "stock" to stockNumber,
            "description" to description
        )

        val product: HashMap<String, Any> = hashMapOf(
            pizza!!.name to productItem
        )
        val dataBase: FirebaseFirestore = Firebase.firestore
        dataBase.collection("Product").document("TnO30fOwkCWYGGGySUQn")
            .update(product)
            .addOnSuccessListener {
                callback(true)
            }
            .addOnFailureListener {
                callback(false)
            }
    }

    fun createNewProduct(id: String, name: String, stock: String, description: String, price: String, callback: (Boolean) -> Unit) {
        val stockNumber: Int = stock.toInt()
        val productItem: HashMap<String, Any> = hashMapOf(
            "name" to name,
            "id" to id,
            "price" to price.toDouble(),
            "stock" to stockNumber,
            "description" to description
        )
        val product: HashMap<String, Any> = hashMapOf(
            name to productItem
        )
        val dataBase: FirebaseFirestore = Firebase.firestore
        dataBase.collection("Product").document("TnO30fOwkCWYGGGySUQn")
            .update(product)
            .addOnSuccessListener {
                callback(true)
            }
            .addOnFailureListener {
                callback(false)
            }
    }
}