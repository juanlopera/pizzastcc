package com.example.pizzas.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pizzas.databinding.PizzaItemBinding
import com.example.pizzas.home.model.Pizza

interface PizzaSelected {
    fun onClickInPizza(position: Int)
}

class PizzaListAdapter(val pizzaList: List<Pizza>): RecyclerView.Adapter<PizzaListViewHolderBase>() {

    private lateinit var listener: PizzaSelected

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PizzaListViewHolderBase {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)
        return PizzaListViewHolderBase.PizzaItem(PizzaItemBinding.inflate(inflater, parent, false), listener)
    }

    override fun onBindViewHolder(holder: PizzaListViewHolderBase, position: Int) {
        val pizza: Pizza = pizzaList[position]
        if (holder is PizzaListViewHolderBase.PizzaItem) {
            holder.bind(pizza)
        }
    }

    override fun getItemCount(): Int {
        return pizzaList.size
    }

    fun setListener(listener: PizzaSelected) {
        this.listener = listener
    }
}