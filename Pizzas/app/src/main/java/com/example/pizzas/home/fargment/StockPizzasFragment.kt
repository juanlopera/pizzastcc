package com.example.pizzas.home.fargment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pizzas.R
import com.example.pizzas.databinding.FragmentStockPizzasBinding
import com.example.pizzas.home.adapter.PizzaStockListAdapter
import com.example.pizzas.home.model.Pizza
import com.example.pizzas.updateAddProduct.view.UpdateAddProductActivity

class StockPizzasFragment(private val pizzaList: MutableList<Pizza>) : Fragment() {

    private lateinit var binding: FragmentStockPizzasBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentStockPizzasBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val pizzasStockAdapter = PizzaStockListAdapter(pizzaList)
        binding.stockList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.stockList.adapter = pizzasStockAdapter
        handleClickEvent()
    }

    private fun handleClickEvent() {
        binding.apply {
            updateStockButton.setOnClickListener {
                navigateToUpdateAddProduct(true)
            }

            addNewProductButton.setOnClickListener {
                navigateToUpdateAddProduct(false)
            }
        }
    }

    private fun navigateToUpdateAddProduct(isUpdating: Boolean) {
        var list: ArrayList<Pizza> = ArrayList()
        pizzaList.forEach { pizza ->
            list.add(pizza)
        }
        val intent: Intent = Intent(requireContext(), UpdateAddProductActivity::class.java)
        intent.putExtra("updating", isUpdating)
        intent.putParcelableArrayListExtra("pizzaUpdating", list)
        startActivity(intent)
    }

}