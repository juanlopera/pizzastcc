package com.example.pizzas.shoppingCart.viewModel

import android.util.Log
import com.example.pizzas.home.model.Pizza
import com.example.pizzas.home.model.Preselection
import com.example.pizzas.sessionManager.SessionManager
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class ShoppingCarViewModel {

    var preselectionList: ArrayList<Preselection> = ArrayList()

    // MARK: - Computed Properties
    var titleError: String = "Erro"
        private set

    var messageError: String = "Ops, aconteceu um erro ao momento de obter a informação do nosso servidor."
        private set

    fun getTotal(): String {
        var total: Double = 0.0
        preselectionList.forEach { preselection ->
            total += (preselection.price * preselection.preselectionAmount.toDouble())
        }
        return "Total: R$ ${total}"
    }

    fun getPreselectedItems(callback: (Boolean) -> Unit) {
        preselectionList = ArrayList()
        val dataBase: FirebaseFirestore = Firebase.firestore
        dataBase.collection("Preselection")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    if (SessionManager.preselectionIds.contains(document.id)) {
                        val preselection: Preselection = Preselection(
                            document.data["name"] as String,
                            (document.data["preselectionAmount"] as Long).toInt(),
                            document.data["idPizza"] as String,
                            document.data["id"] as String,
                            (document.data["stock"] as Long).toInt(),
                            (document.data["price"] as Double)
                        )
                        preselectionList.add(preselection)
                    }
                }
                callback(true)
            }
            .addOnFailureListener {
                callback(false)
            }
    }

    fun deleteShoppingCart() {
        SessionManager.preselectionIds.forEach { idPreselection ->
            val dataBase: FirebaseFirestore = Firebase.firestore
            dataBase.collection("Preselection").document(idPreselection)
                .delete()
                .addOnSuccessListener {
                    SessionManager.preselectionIds.remove(idPreselection)
                    Log.d("", "Preselection deleted")
                }
                .addOnFailureListener {
                    SessionManager.preselectionIds.remove(idPreselection)
                    Log.d("", "Preselection not deleted")
                }
        }
    }
}