package com.example.pizzas.home.viewModel

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.example.pizzas.extension.convertToData
import com.example.pizzas.home.model.Order
import com.example.pizzas.home.model.Pizza
import com.example.pizzas.home.model.Preselection
import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

internal class HomeViewModel {

    // MARK: - Internal Properties
    var pizzaList: ArrayList<Pizza> = ArrayList()
    var preselectionList: ArrayList<Preselection> = ArrayList()
    var orderPizzaList: ArrayList<Order> = ArrayList()

    // MARK: - Computed Properties
    var titleError: String = "Erro"
        private set

    var messageError: String = "Ops, aconteceu um erro ao momento de obter a informação do nosso servidor."
        private set

    // MARK: Internal Function
    fun getOrdersPizza() {
        orderPizzaList = ArrayList()
        val dataBase: FirebaseFirestore = Firebase.firestore
        dataBase.collection("PizzaOrders")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    val order: Order = Order(
                        document.data["date"] as String,
                        document.data["cep"] as String,
                        document.data["customerName"] as String,
                        document.data["status"] as String,
                        document.data["items"] as ArrayList<String>,
                        document.data["paymentMethod"] as String,
                        document.data["total"] as String
                    )
                    orderPizzaList.add(order)
                }
            }
            .addOnFailureListener {
                orderPizzaList = ArrayList()
            }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun getPizzaList(callback: (Boolean) -> Unit, deletingPreselection: () -> Unit) {
        pizzaList = ArrayList()
        preselectionList = ArrayList()
        val dataBase: FirebaseFirestore = Firebase.firestore
        dataBase.collection("Preselection")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    val preselection: Preselection = Preselection(
                        document.data["name"] as String,
                        (document.data["preselectionAmount"] as Long).toInt(),
                        document.data["idPizza"] as String,
                        document.data["id"] as String,
                        (document.data["stock"] as Long).toInt(),
                        (document.data["price"] as Double)
                    )
                    var minutes: Long = 0
                    if (preselection.id.isNotEmpty()) {
                        val remoteDate = preselection.id.convertToData("dd/M/yyyy HH:mm:ss")
                        val sdf = SimpleDateFormat("dd/M/yyyy HH:mm:ss")
                        val localDate = sdf.format(Date()).convertToData("dd/M/yyyy HH:mm:ss")
                        val difference =  localDate.time - remoteDate.time
                        minutes = (difference/1000)/60
                    }
                    if (minutes >= 5) {
                        deletePreselection(document.id)
                        deletingPreselection()
                    } else {
                        this@HomeViewModel.preselectionList.add(preselection)
                    }
                }
                this@HomeViewModel.preselectionList
                getPizzasInformation(dataBase, callback)
            }
            .addOnFailureListener {
                this@HomeViewModel.preselectionList = ArrayList()
                getPizzasInformation(dataBase, callback)
            }
    }

    private fun getPizzasInformation(dataBase: FirebaseFirestore, callback: (Boolean) -> Unit) {
        dataBase.collection("Product")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    for (information in document.data) {
                        val name: String = information.key
                        val id: String = (information.value as HashMap<String, String>).get("id") ?: ""
                        val description: String = (information.value as HashMap<String, String>).get("description") ?: ""
                        val price: Double = (information.value as HashMap<String, Double>).get("price") ?: 0.0
                        val stock: Int = ((information.value as HashMap<String, Long>).get("stock") ?: 0).toInt()
                        val preselectionStock = this@HomeViewModel.getPreselectionAmount(id)
                        val realStock: Int = stock - preselectionStock
                        val pizza: Pizza = Pizza(id, name, description, price, realStock)
                        pizzaList.add(pizza)
                    }
                }
                callback(true)
            }
            .addOnFailureListener {
                callback(false)
            }
    }

    private fun getPreselectionAmount(id: String): Int {
        var preselectionAmount = 0
        if (preselectionList.isNullOrEmpty()) {
            return preselectionAmount
        }
        preselectionList.forEach { preselection ->
            if (preselection.idPizza == id) {
                preselectionAmount = preselectionAmount + preselection.preselectionAmount
            }
        }
        return preselectionAmount
    }

    private fun deletePreselection(documentId: String) {
        val dataBase: FirebaseFirestore = Firebase.firestore
        dataBase.collection("Preselection").document(documentId)
            .delete()
            .addOnSuccessListener {
                Log.d("", "Preselection deleted")
            }
            .addOnFailureListener {
                Log.d("", "Preselection not deleted")
            }
    }
}