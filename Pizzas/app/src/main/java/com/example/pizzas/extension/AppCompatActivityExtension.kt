package com.example.pizzas.extension

import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity

fun AppCompatActivity.configureBackButtonWith(navigationTitle: String) {
    val actionBar: ActionBar? = supportActionBar
    actionBar?.let { bar ->
        bar.apply {
            title = navigationTitle
            setDisplayHomeAsUpEnabled(true)
        }
    }
}