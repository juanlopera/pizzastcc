package com.example.pizzas.login.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import androidx.appcompat.app.AlertDialog
import com.example.pizzas.databinding.ActivityMainBinding
import com.example.pizzas.home.view.HomeActivity
import com.example.pizzas.login.viewModel.MainViewModel
import com.example.pizzas.signup.view.SignupActivity

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel = MainViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.loginButton.setOnClickListener {
            val email: String = binding.usernameEditText.text.toString()
            val password: String = binding.signupPasswordEditText.text.toString()
            viewModel.validateUserCredentials(email, password) { result ->
                if (result) {
                    val intent: Intent = Intent(this, HomeActivity::class.java)
                    startActivity(intent)
                } else {
                    showMessage(viewModel.title, viewModel.message)
                }
            }

        }

        binding.signupButton.setOnClickListener {
            val intent: Intent = Intent(this, SignupActivity::class.java)
            startActivity(intent)
        }
    }

    private fun showMessage(title: String, message: String) {
        val builderDialog: AlertDialog.Builder = AlertDialog.Builder(this)
        builderDialog.setTitle(title)
        builderDialog.setMessage(message)
        builderDialog.setNeutralButton("Entendi") { _, _, ->
            binding.apply {
                usernameEditText.setText("")
                signupPasswordEditText.setText("")
            }
        }
        val dialog: AlertDialog = builderDialog.create()
        dialog.show()
    }

}